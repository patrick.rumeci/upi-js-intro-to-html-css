# UPI - Web Programing 101 - Javascript Introduction

This repository is a scaffold of what to submit for the coding challenges of this course.

# Create a New Project in GitLab.com

To create a new project, click on Projects on the GitLab's page then click on Your projects. It will take you to a page that contains different options regarding projects. Since you have not created a project yet, you will see the option Create a Project. Click on that. It will take you to a New Project page. Scroll down to the form for creating a blank project. Write the appropriate project name. This will automatically enter the same name under the project slug. For the visibility level, select public level to ensure that the project can be assessed easily. Since you are expected to push up an existing repository, leave the square next to Initialize repository with a README empty. Then click on the Create Project button. You now have a new and blank project.

# Copy the Repository to the Local Machine

To copy the repository (E.g.: The repository of UPI JS Intro to HTML CSS from the user Patrick) to your local machine, specifically to the default root directory of your http server, type in Git Bash the following command:

````
git clone git@gitlab.com:patrick.rumeci/upi-js-intro-to-html-css.git
````

This will save the folder onto the directory of your http server.

# Edit the HTML File

Step 1: To access the html file in your favorite browser, type the following address:

```
http://localhost/upi-js-intro-to-html-css/write-a-poem.html
```

Note: You can also view this page to test the different changes you made to the html file when accessing it with Atom.

Step 2: Once you have finished editing the HTML file, save it.


# Commit the Changes in the Files using Git GUI

Step 1: Open Git GUI. This will bring a window of Git GUI.

Step 2: Click on the Create New Repository since this is the first time that you are creating a repository.

Step 3: In the directory, select the UPI JS Intro to HTML CSS folder that is in your http server's default root directory.

Step 4: Click on the create button. This will take you to another window.

Step 5: The file that you modified will be listed under unstaged changes. On its right, you will see the lines of code from the file you previously worked on. It will show all changes that you did. To commit the changes, you will simply click the Stage Changed button. This will move the file from unstage changes to staged changes. The file is ready to have its changes committed.

Step 6: Click on the Commit button after writing the comment for the commit.

# Push the Repository to GitLab Account Online

You are ready to push the repository to your Gitlab account. To do that, go to Git Bash.

Step 1: Change the directory if you have not done it yet.

````
cd ./upi-js-intro-to-html-css
````

Step 2: Rename the existing remote (It is now the old origin instead of being the origin)

````
git remote rename origin old-origin
````

Step 3: Add the new remote to the directory where the repository is stored at.

````
git remote add origin git@gitlab.com:username/upi-js-intro-to-html-css.git
````

Step 4: Push all the files in the new remote to the repository.  

````
git push -u origin --all
````

Step 5: Push all of your local branches to the specified remote.

````
git push -u origin --tags
````

Congratulations! You have created your project on Gitlab project.

# Exit Git Bash

To exit from Git Bash, simply type exit.
